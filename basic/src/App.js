import React from 'react';
import logo from './logo.svg';
import './App.css';
import Greet from'./component/Greet';
import Welcome from'./component/Welcome';
import Message from'./component/Message';
import Counter from'./component/Counter';
import FunctionClick from './component/FunctionClick';
import ClassClick from './component/ClassClick';
import EventBind from './component/EventBind';
import ParentComponent from './component/ParentComponent';
import UserGreeting from './component/UserGreeting';
import NameList from './component/NameList';
import Stylesheet from './component/Stylesheet';
import Inline from './component/Inline';
import Form from './component/Form';
import LifecycleA from './component/LifecycleA';
import FragmentDemo from './component/FragmentDemo';
import Table from './component/Table';
import PureComponent from './component/PureComp';
import ParentComp from './component/ParentComp';
import RefDemo from './component/RefDemo';
import FocusInput from './component/FocusInput';
import FRParentInput from './component/FRParentInput';
import Hero from './component/Hero';
import ErrorBound from './component/ErrorBound';
import PortalDemo from './component/PortalDemo';
import ClickCounter from './component/ClickCounter';
import HoverCounter from './component/HoverCounter';
function App() {
  return (
   
    <div className="App">
      <ClickCounter name='Arpit'/>
<HoverCounter/>
   { /*<LifecycleA/><PortalDemo/>  <FocusInput/><FRParentInput/> <RefDemo/><ParentComp/><PureComponent/> <Table/>  <FragmentDemo/><LifecycleA/> <Form /><Stylesheet />    <Inline/> <NameList/>   <UserGreeting /> <EventBind />  <ParentComponent />*/}
       { /*       <ErrorBound>
<Hero heroname="Batman"/>
</ErrorBound>
<ErrorBound>
<Hero heroname="Superman"/>
</ErrorBound>
<ErrorBound>
<Hero heroname="joker"/>
</ErrorBound>
         <FunctionClick />  <ClassClick />
     <Greet name='Arpit' heroname='sir'/>
      <Greet name='gouri'/>   <Counter/>  <Welcome />
   */}
    </div>
  );
}

export default App;
