import React, { Component } from 'react'
import LifecycleB from './LifecycleB'

class LifecycleA extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name:"vishaw"
        }
        console.log("Life Cylce A Constructure")
    }
static getDerivedStateFromProps(props,state){
        console.log("getDerivedStateFromProps LifecycleA")
        return null;
    }
componentDidMount(){
        console.log("Component didmount lifeCycle A")
    }
shouldComponentUpdate(){
 console.log("shouldComponentUpdate Life Cycle A")  
 return true 
}
getSnapshotBeforeUpdate(prevProps,prevState){
 console.log("getSnapshotBeforeUpdate lifecycle A")
 return null
}
componentDidUpdate(){
        console.log("componentsDidUpdate Life Cycle A")
    }
changeState=()=>{
        this.setState({
            name:"Code Evolution"
        })
    }
render() {
        console.log("render method Life CycleA")
        return (
            <div>
                 <div>
                LifecycleA 
                </div>
                <button onClick={this.changeState}>change State</button>
                <LifecycleB />
            </div>
           
        )
    }
}

export default LifecycleA
