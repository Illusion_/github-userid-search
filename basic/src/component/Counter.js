import React, { Component } from 'react'

export class Counter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count:0
        }
    }
    counter(){
 /*this.setState({
     count:this.state.count + 1
 },()=>{console.log('call by value',this.state.count)} )*/
this.setState(prev =>({
    count:prev.count+1
}))

console.log(this.state.count)
    }
    incrementFive(){
        this.counter();
        this.counter();
        this.counter();
    }
    render() {
        return (
            <div>
              <div>  Count- {this.state.count}</div>
                <button onClick={() => this.incrementFive()}>Counter</button>
            </div>
        )
    }
}

export default Counter
