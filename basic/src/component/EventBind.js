import React, { Component } from 'react'

 class EventBind extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
              message:"hello"
         }
     }
     clickhandler(){
         this.setState({
             message:"bye"
         })
        }
    render() {
        return (
            <div>
                <div>{this.state.message}</div>
                <button onClick={this.clickhandler.bind(this)}>Click</button>
            </div>
        )
    }
}

export default EventBind
