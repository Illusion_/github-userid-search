import React, { Component } from 'react'

export class ClassClick extends Component {
    clickhandler(){
     console.log("Cklick on Class")       
    }
    render() {
        return (
            <div>
                <button onClick={this.clickhandler}>Click me</button>
            </div>
        )
    }
}

export default ClassClick
