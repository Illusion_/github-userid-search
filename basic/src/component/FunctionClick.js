import React from 'react'

function FunctionClick() {
    function  ClickButton(){
        console.log("Button cLicked function")
    }
    return (
        <div>
            <button onClick={ClickButton}>Click</button>
        </div>
    )
}

export default FunctionClick
