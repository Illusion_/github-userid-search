import React, { Component } from 'react'

class LifecycleB extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            name:"vishaw"
        }
        console.log("Life Cylce B Constructure")
    }
    static getDerivedStateFromProps(props,state){
        console.log("getDerivedStateFromProps life cycle B")
        return null;
    }
    componentDidMount(){
        console.log("Component didmount lifeCycle B")
    }
    shouldComponentUpdate(){
        console.log("shouldComponentUpdate Life Cycle B")  
        return true 
       }
           getSnapshotBeforeUpdate(prevProps,prevState){
        console.log("getSnapshotBeforeUpdate lifecycle B")
        return null
           }
           componentDidUpdate(){
               console.log("componentsDidUpdate Life Cycle B")
           }
        
    render() {
        console.log("render method Life CycleB")
        return (
            <div>
                LifecycleB 
               
        </div>
        )
    }
}

export default LifecycleB
