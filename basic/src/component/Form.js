import React, { Component } from 'react'
 class Form extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
              username:'',
              comments:'',
              topic:'react'
         }
     }
     handleUsernameChange= (event)=>{
this.setState({
    username:event.target.value
})
     }
    handleCommentChange=(event)=>{
         this.setState({
             comments:event.target.value
         })
     }
     handleTopicChange=(event)=>{
   this.setState({
       topic:event.target.value
   })
     }
     handleSubmit=(event)=>{
         alert(`username- ${this.state.username} \n comments-  ${this.state.comments} \n topic-  ${this.state.topic} \n`)
        event.preventDefault() 
        }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
            <div>
                <label>User Name</label>
                <input type='text' value={this.state.username}  onChange={this.handleUsernameChange}/>
            </div>
            <div>
                <label>Comments</label>
                <textarea value={this.state.comments} onChange={this.handleCommentChange}></textarea>
            </div>
            <div>
                <label>Topic Area</label>
                <select value={this.state.topic} onChange={this.handleTopicChange}>
                    <option value='react'>React</option>
                    <option value='angular'>Angular</option>
                    <option value='vue'>Vue</option>
                </select>
            </div>
            <button type="submit">Submit</button>
            </form>
        )
    }
}

export default Form
