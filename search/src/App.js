import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'; 
import Front from './components/Front';


function App() {
  return (
    <div className="App">
      <Front/>
    </div>
  );
}

export default App;
